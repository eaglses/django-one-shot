from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import NewListForm, NewListItem


# Create your views here.
def todo_list_list(request):
    todolist = TodoList.objects.all()
    context = {"todolist": todolist}
    return render(request, "todos/list_list.html", context)


# details view
def todo_list_detail(request, id):
    details = TodoList.objects.get(id=id)
    context = {
        "list_object": details,
    }
    return render(request, "todos/details.html", context)


# add to your list
def create_list(request):
    if request.method == "POST":
        form = NewListForm(request.POST)
        if form.is_valid():
            new_list = form.save(False)
            new_list.save()
            return redirect("todo_list_detail", id=new_list.id)
    else:
        form = NewListForm()

        context = {"form": form}
    return render(request, "todos/NewListForm.html", context)


# edit the list
def edit_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = NewListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
        return redirect("todo_list_detail", id=id)
    else:
        form = NewListForm(instance=list)
        context = {"list_object": list, "list_form": form}
    return render(request, "todos/edit_list.html", context)


# delet an instance
def delete_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def add_item(
    request,
):
    if request.method == "POST":
        form = NewListItem(request.POST)
        if form.is_valid():
            new_list = form.save(False)
            new_list.save()
            return redirect("todo_list_detail", id=new_list.list.id)
    else:
        form = NewListItem()

        context = {"form": form}
    return render(request, "todos/NewItemForm.html", context)


# edit the item
def edit_item(request, id):
    list = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = NewListItem(request.POST, instance=list)
        if form.is_valid():
            form.save()
        return redirect("todo_list_detail", id=list.list.id)
    else:
        form = NewListItem(instance=list)
        context = {"item_object": list, "item_form": form}
    return render(request, "todos/item_edit.html", context)
